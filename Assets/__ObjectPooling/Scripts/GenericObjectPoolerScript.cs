﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenericObjectPoolerScript : MonoBehaviour
{
	public static GenericObjectPoolerScript m_genericObjectPoolerScript;
	public GameObject m_pooledObject;
	public int m_pooledAmount = 20;
	public bool m_willGrow = true;

	private List<GameObject> m_pooledObjects;

	private void Awake()
	{
		m_genericObjectPoolerScript = this;
	}

	// Use this for initialization
	void Start ()
	{
		PoolObjects();
	}

	private void PoolObjects()
	{
		m_pooledObjects = new List<GameObject>();
		for (int i = 0; i < m_pooledAmount; i++)
		{
			GameObject pooledObject = (GameObject) Instantiate(m_pooledObject);
			pooledObject.SetActive(false);
			m_pooledObjects.Add(pooledObject);
		}
	}

	public GameObject GetPooledObject()
	{
		for (int i = 0; i < m_pooledObjects.Count; i++)
		{
			if (!m_pooledObjects[i].activeInHierarchy)
			{
				return m_pooledObjects[i];
			}
		}

		if (m_willGrow)
		{
			GameObject pooledObject = (GameObject)Instantiate(m_pooledObject);
			m_pooledObjects.Add(pooledObject);
			return pooledObject;
		}

		return null;
	}
}
