﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletFireScript : MonoBehaviour
{
	public float m_fireTime = 0.05f;

	// Use this for initialization
	void Start ()
	{
		InvokeRepeating("Fire", m_fireTime, m_fireTime);
	}

	void Fire()
	{
		IfThereIsAPooledObjectFireIt();
	}

	private void IfThereIsAPooledObjectFireIt()
	{
		GameObject gameObjectToFire = GenericObjectPoolerScript.m_genericObjectPoolerScript.GetPooledObject();

		if (gameObjectToFire != null)
		{
			SetPositionAndRotationAndActiveTrue(gameObjectToFire);
		}
	}

	private void SetPositionAndRotationAndActiveTrue(GameObject pooledGameObject)
	{
		pooledGameObject.transform.position = transform.position;
		pooledGameObject.transform.rotation = transform.rotation;
		pooledGameObject.SetActive(true);
		return;
	}
}
