﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
	public float m_speed = 5f;

	// Update is called once per frame
	void Update ()
	{
		MoveVerticallyWithSpeed();
	}

	private void MoveVerticallyWithSpeed()
	{
		transform.Translate(0, m_speed * Time.deltaTime, 0);
	}
}
